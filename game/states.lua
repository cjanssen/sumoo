-- Copyright 2013 Kirill Krysov and Christiaan Janssen
-- This file is part of Sumoo.
--
--    Sumoo is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    Sumoo is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with Sumoo.  If not, see <http://www.gnu.org/licenses/>.
States = {}
function States.clear(S)
	S.list = {}
	S.current = nil
end

function States.newState(S, name_, init_, draw_, update_, checkChange_, keypressed_, mousepressed_)
	S.list[name_] = {
		init = init_,
		name = name_,
		draw = draw_,
		update = update_,
		checkChange = checkChange_,
		keypressed = keypressed_,
		mousepressed = mousepressed_
	}
	return S.list[name_]
end

function States.setCurrent(S, name)
	S.current = name
	if S.list[name].init then
		S.list[name].init()
	end
end

function States.setExitOnTimeout(S, state, newstate, time)
	local stateRef = S.list[state]
	local initFunc = stateRef.init
	stateRef.init = function()
		stateRef.timer = time
		if initFunc then initFunc() end
	end

	local updFunc = stateRef.update
	stateRef.update = function(dt)
		stateRef.timer = stateRef.timer - dt
		if updFunc then updFunc(dt) end
	end

	local cCFunc = stateRef.checkChange
	stateRef.checkChange = function()
		if stateRef.timer <= 0 then return newstate end
		if cCFunc then cCFunc() end
	end
end

function States.setExitOnPress(S, state, newstate, key)
	local stateRef = S.list[state]
	local initFunc = stateRef.init
	stateRef.init = function()
		stateRef.keyToExit = key
		stateRef.exitFlag = false
		if initFunc then initFunc() end
	end

	local kpFunc = stateRef.keypressed
	stateRef.keypressed = function(key_)
		if stateRef.keyToExit then
			if stateRef.keyToexit == key then stateRef.exitFlag = true end
		else
			stateRef.exitFlag = true
		end
		if kpFunc then kpFunc() end
	end

	local cCFunc = stateRef.checkChange
	stateRef.checkChange = function()
		if stateRef.exitFlag then return newstate end
		if cCFunc then cCFunc() end
	end
end

function States.setExitOnPressOrMousedown(S, state, newstate, key)
	local stateRef = S.list[state]
	local initFunc = stateRef.init
	stateRef.init = function()
		stateRef.keyToExit = key
		stateRef.exitFlag = false
		if initFunc then initFunc() end
	end

	local kpFunc = stateRef.keypressed
	stateRef.keypressed = function(key_)
		if stateRef.keyToExit then
			if stateRef.keyToexit == key then stateRef.exitFlag = true end
		else
			stateRef.exitFlag = true
		end
		if kpFunc then kpFunc() end
	end

	stateRef.mousepressed = function(x_, y_)
		stateRef.exitFlag = true
	end

	local cCFunc = stateRef.checkChange
	stateRef.checkChange = function()
		if stateRef.exitFlag then return newstate end
		if cCFunc then cCFunc() end
	end
end

function States.draw(S)
	if S.current and S.list[S.current].draw then
		S.list[S.current].draw()
	end
end

function States.update(S, dt)
	if S.current then
		if S.list[S.current].update then
			S.list[S.current].update(dt)
		end
		if S.list[S.current].checkChange then
			local ns = S.list[S.current].checkChange()
			if ns and ns ~= S.list[S.current].name then
				States.setCurrent(S, ns)
			end
		end
	end
end

function States.keypressed(S,key)
	if S.current and S.list[S.current].keypressed then
		S.list[S.current].keypressed(key)
	end
end

function States.mousepressed(S,x,y)
	if S.current and S.list[S.current].mousepressed then
		S.list[S.current].mousepressed(x,y)
	end
end
