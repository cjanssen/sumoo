-- Copyright 2013 Kirill Krysov and Christiaan Janssen
-- This file is part of Sumoo.
--
--    Sumoo is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    Sumoo is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with Sumoo.  If not, see <http://www.gnu.org/licenses/>.
Sounds = {}

function Sounds.load()
	Sounds.arena = love.audio.newSource("snd/30479_arena.mp3")
	Sounds.bar = love.audio.newSource("snd/333655_bar.mp3")
	Sounds.steak = love.audio.newSource("snd/380763_steak.mp3")
end

function Sounds.stopAll()
	Sounds.arena:setVolume(0)
	Sounds.arena:stop()
	Sounds.bar:setVolume(0)
	Sounds.bar:stop()
	Sounds.steak:setVolume(0)
	Sounds.steak:stop()
end

function Sounds.play(s)
	Sounds.stopAll()
	s:setVolume(1)
	s:play()
end
