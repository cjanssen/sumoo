-- Copyright 2013 Kirill Krysov and Christiaan Janssen
-- This file is part of Sumoo.
--
--    Sumoo is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    Sumoo is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with Sumoo.  If not, see <http://www.gnu.org/licenses/>.
GameScenes = {}

function GameScenes.init()
	GameScenes.Images = {
		entrance = love.graphics.newImage("img/enterance.png"),
		title = love.graphics.newImage("img/title.png"),
		lose_color = love.graphics.newImage("img/lost_fighter.png"),
		lose_gray = love.graphics.newImage("img/lost_dessat.png"),
		win = love.graphics.newImage("img/game-over.png"),
		bar = love.graphics.newImage("img/bar.png"),
		cont = {
			logo = love.graphics.newImage("img/continue.png"),
			zero = love.graphics.newImage("img/zero.png"),
			one = love.graphics.newImage("img/one.png"),
			two = love.graphics.newImage("img/two.png"),
			three = love.graphics.newImage("img/three.png")
		}
	}

	TalkAnim = {
		timer = 0,
		period = 0.7,
		frames = {
			love.graphics.newImage("img/bluebeltcow_1.png"),
			love.graphics.newImage("img/bluebeltcow_2.png")
		},
		frameIndex = 1
	}

	GameScenes.states = {}
	States.clear(GameScenes.states)
	GameScenes.initStates()
end

function GameScenes.draw()
	States.draw(GameScenes.states)
end

function GameScenes.update(dt)
	States.update(GameScenes.states, dt)
end

function GameScenes.keypressed(key)
	States.keypressed(GameScenes.states, key)
end

function GameScenes.mousepressed(x,y)
	States.mousepressed(GameScenes.states, x, y)
end

function GameScenes.initStates()
	States.newState(GameScenes.states, "start", 
		-- init
		function()
			Texts.clear()
			Texts.appendLabel("by Kirill K. and Christiaan J.\nBerlin Mini Game Jam - June 2013\n\nClick to start game")
			gameDifficulty = 0.10
			Sounds.play(Sounds.steak)
			Insults.init()
		end,
		--draw,
		function() 
			love.graphics.setColor(255,255,255)
			love.graphics.draw(GameScenes.Images.entrance,0,0,0,2,2) 
			love.graphics.draw(GameScenes.Images.title,0,0,0,2,2)
		end,
		-- update,
		nil,
		--checkChange
		nil
	)
	States.setExitOnPressOrMousedown(GameScenes.states, "start","intro")
	States.newState(GameScenes.states, "intro", 
		-- init
		function()
			Texts.clear()
			Texts.appendLabel("Kobe, Japan.  1998.")
		end,
		--draw,
		function() 
			love.graphics.setColor(255,255,255)
			love.graphics.draw(GameScenes.Images.entrance,0,0,0,2,2)
		end,
		-- update,
		nil,
		--checkChange
		nil
	)
	States.setExitOnTimeout(GameScenes.states, "intro","talk",2.5)

	States.newState(GameScenes.states, "talk", 
		-- init
		function()
			Texts.clear()
			Insults.newRound()
			Insults.ask()
			Sounds.play(Sounds.bar)	
		end,
		--draw,
		function() 
			love.graphics.setColor(255,255,255)
			love.graphics.draw(GameScenes.Images.bar,0,0,0,2,2)
			love.graphics.draw(TalkAnim.frames[TalkAnim.frameIndex],0,0,0,2,2)	
			Insults.draw()
		end,
		-- update,
		function(dt)
			TalkAnim.timer = TalkAnim.timer + dt
			if TalkAnim.timer >= TalkAnim.period then
				TalkAnim.timer = TalkAnim.timer - TalkAnim.period
				TalkAnim.frameIndex = TalkAnim.frameIndex % table.getn(TalkAnim.frames) + 1
			end
			Insults.update(dt)
		end, 
		--checkChange
		function()
			if Insults.winner > 0 then
				return "fight"
			end
		end,
		--keypressed
		function(key)
			local number = 0
			if key == "1" then Insults.evalOption(1) end
			if key == "2" then Insults.evalOption(2) end
			if key == "3" then Insults.evalOption(3) end
			if key == "space" then Insults.scores[1] = 3 end
		end,
		--mousepressed
		nil
	)

	States.newState(GameScenes.states, "fight", 
		-- init
		Fight.init,
		--draw,
		Fight.draw,
		-- update,
		Fight.update, 
		--checkChange
		Fight.checkEnd
	)

	States.newState(GameScenes.states, "win",
		-- init
		function()
			Texts.clear()
			Texts.appendLabel("You won the tournament!  Congratulations!\nthe prize... is you. Served with a glass of red wine.")
		end,
		--draw,
		function()
			love.graphics.setColor(255,255,255)
			love.graphics.draw(GameScenes.Images.win,0,0,0,2,2)
		end,
		-- update,
		nil, 
		--checkChange
		nil
	)

	States.setExitOnTimeout(GameScenes.states,"win","start",7)

	States.newState(GameScenes.states, "lose",
		-- init
		function()
			Texts.clear()
			Texts.appendLabel("You lose the fight")
			lose_opacity = 1
			continue_condition = false
			lose_timer = 0
			continue_number = 3
		end,
		--draw,
		function()
			if lose_timer < 1.5 then
				love.graphics.setColor(255,255,255,255*lose_opacity)
				love.graphics.draw(Fight.Images.arena,0,0,0,2,2)
				love.graphics.draw(Fight.Images.cheering_cows[Fight.cheering.index], 0, 0, 0, 2, 2)
				love.graphics.draw(Fight.Images.end_fight_2, 0, 0, 0, 2, 2)
				love.graphics.setColor(255,255,255)
				love.graphics.draw(GameScenes.Images.lose_gray,0,0,0,2,2)
				love.graphics.setColor(255,255,255,255*lose_opacity)
				love.graphics.draw(GameScenes.Images.lose_color,0,0,0,2,2)
			else
				if lose_timer > 4.5 then
					love.graphics.setColor(255,255,255,255*lose_opacity)
				else
					love.graphics.setColor(255,255,255)
				end
				love.graphics.draw(GameScenes.Images.lose_gray,0,0,0,2,2)
				if continue_condition then
					love.graphics.setColor(255,255,255,255*(1-lose_opacity))
					love.graphics.draw(GameScenes.Images.lose_color,0,0,0,2,2)
				end
				love.graphics.setColor(255,255,255,255*lose_opacity)
				love.graphics.draw(GameScenes.Images.cont.logo,0,0,0,2,2)
				if lose_number == 3 then
					love.graphics.draw(GameScenes.Images.cont.three,0,0,0,2,2)
				elseif lose_number == 2 then
					love.graphics.draw(GameScenes.Images.cont.two,0,0,0,2,2)
				elseif lose_number == 1 then
					love.graphics.draw(GameScenes.Images.cont.one,0,0,0,2,2)
				elseif lose_number == 0 then
					love.graphics.draw(GameScenes.Images.cont.zero,0,0,0,2,2)
				end
			end
		end,
		-- update,
		function(dt)
			lose_opacity = decreaseExponential(dt,lose_opacity,0.97)
			lose_timer = lose_timer + dt
			if lose_timer > 1.5 and not continue_condition then
				if lose_timer < 2.5 then
					lose_number = 3
				elseif lose_timer < 3.5 then
					lose_number = 2
				elseif lose_timer < 4.5 then
					lose_number = 1
				elseif lose_timer < 6.5 then
					lose_number = 0
				end
				if lose_timer > 5.5 then
					lose_opacity = decreaseExponential(dt,lose_opacity,0.985)
				else
					lose_opacity = 1
				end
			else
				lose_opacity = decreaseExponential(dt,lose_opacity,0.985)
			end
		end, 
		--checkChange
		function()
			if continue_condition and lose_timer > 3 then return "talk" end
			if lose_timer > 7 then return "start" end
		end,
		--keypressed
		function(key)
			if lose_timer > 1.5 and lose_timer < 4.5 and not continue_condition then
				continue_condition = true
				lose_timer = 1.5
			end
		end,
		--mousepressed
		function()
			if lose_timer > 1.5 and lose_timer < 4.5 and not continue_condition then
				continue_condition = true
				lose_timer = 1.5
			end
		end 
	)

--	States.setExitOnTimeout(GameScenes.states,"lose","start",6.5)

	States.setCurrent(GameScenes.states, "start")
end
