-- Copyright 2013 Kirill Krysov and Christiaan Janssen
-- This file is part of Sumoo.
--
--    Sumoo is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    Sumoo is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with Sumoo.  If not, see <http://www.gnu.org/licenses/>.
Fight = {}

function Fight.init()
	Texts.clear()
	Fight.Images = {
		arena = love.graphics.newImage("img/arena.png"),
		cheering_cows = { 
			love.graphics.newImage("img/cheering_cows_1.png"),
			love.graphics.newImage("img/cheering_cows_2.png") 
		} ,
		start_fight = {
			love.graphics.newImage("img/start_fight_1.png"),
			love.graphics.newImage("img/start_fight_2.png") 
		} ,
		fight = { 
			love.graphics.newImage("img/fight_1.png"),
			love.graphics.newImage("img/fight_2.png") 
		},
		end_fight_1 = love.graphics.newImage("img/end_fight_1.png"),
		end_fight_2 = love.graphics.newImage("img/end_fight_2.png"),
	}

	Fight.cheering = {
		index = 1,
		timer = 0,
		frameDelay = 0.5 
	}

	Fight.startAnim = {
		index = 1,
		timer = 0,
		frames = {1,2,1,2,1,2,1},
		times =  {1,0.7,1,0.7,1,0.7,1.8},
		images = Fight.Images.start_fight,
		done = false
	}

	Fight.fightAnim = {
		index = 1,
		timer = 0,
		frames = {1,2},
		times = {1,1},
		images = Fight.Images.fight,
		done = false
	}
--	if Insults.scores[1] >= Insults.scores[2] then
	if Insults.winner == 1 then
		Fight.Images.winner = {Fight.Images.end_fight_1}
	else
		Fight.Images.winner = {Fight.Images.end_fight_2}
	end
	Fight.winAnim = {
		index = 1,
		timer = 0,
		frames = {1},
		times = {4},
		images = Fight.Images.winner,
		done = false
	}
	Fight.fightIters = 0
	Fight.animToShow = 1

	Sounds.play(Sounds.arena)
end

function Fight.updateAnim(anim, dt)
	anim.timer = anim.timer + dt
	if anim.timer > anim.times[anim.index] then
		anim.timer = anim.timer - anim.times[anim.index]
		anim.index = anim.index + 1
		if anim.index > table.getn(anim.frames) then
			anim.index = 1
			anim.done = true
			return true
		end
	end
	return false
end

function Fight.currentFrame(anim)
	return anim.images[anim.frames[anim.index]]
end

function Fight.update(dt)
	Fight.cheering.timer = Fight.cheering.timer + dt
	if Fight.cheering.timer > Fight.cheering.frameDelay then
		Fight.cheering.timer = Fight.cheering.timer - Fight.cheering.frameDelay
		Fight.cheering.index = Fight.cheering.index % 2 + 1
	end

	if not Fight.startAnim.done then
		Fight.animToShow = 1
		Fight.updateAnim(Fight.startAnim, dt)
	elseif Fight.fightIters < 15 then
		Fight.animToShow = 2
		if Fight.updateAnim(Fight.fightAnim, dt) then
			Fight.fightIters = Fight.fightIters + 1
			for i=1,table.getn(Fight.fightAnim.times) do
				Fight.fightAnim.times[i] = math.max(0.1, Fight.fightAnim.times[i] * 0.8)
			end
		end
	else
		-- show winner
		Fight.updateAnim(Fight.winAnim, dt)
		if Fight.animToShow == 2 then
			Texts.clear()
			if Insults.winner == 1 then
				Texts.appendLabel("You win the fight")
			else
				Texts.appendLabel("You lose the fight")
			end
		end
		Fight.animToShow = 3
		if Fight.winAnim.done then Fight.animToShow = 4 end
	end
end

function Fight.draw()
	
	love.graphics.setColor(255,255,255)
	love.graphics.draw(Fight.Images.arena,0,0,0,2,2)
	love.graphics.draw(Texts.background, 0, 400, 0, 2, 2)

	-- cheering
	love.graphics.draw(Fight.Images.cheering_cows[Fight.cheering.index], 0, 0, 0, 2, 2)

	if Fight.animToShow == 1 then
		love.graphics.draw(Fight.currentFrame(Fight.startAnim), 0, 0, 0, 2, 2)
	elseif Fight.animToShow == 2 then
		love.graphics.draw(Fight.currentFrame(Fight.fightAnim), 0, 0, 0, 2, 2)
	else
		love.graphics.draw(Fight.currentFrame(Fight.winAnim), 0, 0, 0, 2, 2)
	end
end

function Fight.checkEnd()
	if Fight.animToShow == 4 then
		if Insults.winner == 2 then
			return "lose"
		else
			if Insults.atEnd() then
				return "win"
			end
		end
		return "talk" 
	end
end
