-- Copyright 2013 Kirill Krysov and Christiaan Janssen
-- This file is part of Sumoo.
--
--    Sumoo is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    Sumoo is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with Sumoo.  If not, see <http://www.gnu.org/licenses/>.
function include( filename )
	love.filesystem.load( filename )()
end

function love.load()
	include("utils.lua")
	include("texts.lua")
	include("insults.lua")
	include("states.lua")
	include("gamescenes.lua")
	include("fight.lua")
	include("sound.lua")
	Sounds.load()
	initFont()
	Texts.load()
	GameScenes.init()
	Insults.init()
	gameDifficulty = 0.10
end

function initFont()
	game = {}
	game.font = love.graphics.newFont("fnt/DisposableDroidBB.ttf", 24)
	love.graphics.setFont(game.font)
end

function love.draw()
	GameScenes.draw()
	Texts.draw()
end

function love.update(dt)
	if dt > 0.5 then return end -- skip frames below 2FPS
	GameScenes.update(dt)
	Texts.update(dt)
end

function love.keypressed(key)
	if key == "escape" then
		love.event.push("quit")
		return
	end
	GameScenes.keypressed(key)
end

function love.mousepressed(x,y)
	GameScenes.mousepressed(x,y)
end

