-- Copyright 2013 Kirill Krysov and Christiaan Janssen
-- This file is part of Sumoo.
--
--    Sumoo is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    Sumoo is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with Sumoo.  If not, see <http://www.gnu.org/licenses/>.
Insults = {}

function Insults.init()
	Insults.getData()
	Texts.defaultCallback = Insults.evalOption
	Insults.currentRound = 0
	Insults.currentStep = 1
	Insults.winner = 0
end

function Insults.atEnd()
	return Insults.currentRound == table.getn(Insults.data)
end

function Insults.newRound()
	Insults.timer = 0
	Insults.scores = {0,0}	
	Insults.initDelay = 1.5
	if Insults.winner ~= 2 then
		Insults.currentRound = Insults.currentRound + 1
	end
	Insults.winner = 0
	Insults.currentStep = 1
end

function Insults.ask()
	Insults.phase = 0
	Insults.timer = 0
	Texts.clear()
end

function Insults.evalOption(n)
	if Insults.phase ~= 2 then return end
	local db = Insults.data[Insults.currentRound][Insults.currentStep][3]
	if n > table.getn(db) then return end
	
	Insults.timer = 0
	Insults.phase = 3
	Insults.initDelay = 0
	for i=1,table.getn(Texts.UIlist) do
		if i-1 ~= n then
			Texts.UIlist[i].shadeFactor = 0
		else
			Texts.UIlist[i].selected = true
		end
	end

	Insults.currentStep = db[n][1]
	
end

function Insults.draw()
--	Texts.draw()
end

function Insults.update(dt)
	Insults.timer = Insults.timer + dt
	if Insults.phase == 0 and Insults.timer > Insults.initDelay then
		local db = Insults.data[Insults.currentRound][Insults.currentStep]
		Texts.appendLabel(db[2],{255,128,128})
		Insults.phase = 1
	elseif Insults.phase == 1 and Insults.timer > 3.5 + Insults.initDelay then
		local db = Insults.data[Insults.currentRound][Insults.currentStep]
		for i,v in ipairs(db[3]) do
			Texts.appendButton(i..") "..v[2],i)
		end
		Insults.phase = 2
	elseif Insults.phase == 3 and Insults.timer > 2.5 then
		if (Insults.currentStep < 0) then
			-- fight
			Insults.winner = -Insults.currentStep
		else
			Insults.ask()
		end
	end
end

function Insults.getData()
	Insults.data = {
		{ -- round 1
		{1, "... and you are going to be my rival tonight?\nI hope you are prepared.  I am going to beat you.",
			{
			{2,"I was born prepared"},
		 	{3,"You couldn't beat a butterfly"} 
			} },
		{2, "You seem very sure of yourself.  How often do you train?",
			{
			{4,"I don't need to train against mosquitoes like you"},
			{5,"I wonder if you train at all"},
			{6,"Training? That's for cowards"}
			} },
		{3, "I am stronger than you think.  Don't underestimate me.",
			{
			{7,"You are the one underestimating me"},
			{8,"You talk too much, do you need to prove something?"},
			} },
		{4, "A mosquito can bite quite bad, you know?",
			{
			{-1,"So, you accept that you are a mosquito?"},
			{-2,"If biting is all you can do you've lost already"},
			{-1,"Biting is forbidden in sumo"}
			} },
		{5,"I am hitting the gym every day.  This is all muscle.",
			{
			{-1,"You mean that this is all fat"},
			{-1,"The gym doesn't seem to do you any effect"},
			{-2,"Still, all that 'muscle' will not save you"},
			} },
		{6,"That overconfidence will betray you sooner than you think.",
			{
			{-2,"I am the best and you know it"},
			{-2,"You seem to be one of those cowards I'm talking about"},
			{-1,"Well, wise cow, show me what you got"},
			} },
		{7,"Let's see who is underestimating whom in the ring.",
			{
			{-2,"Bring it on"},
			{-2,"You are going to lose and you know it"},
			{-1,"You will have to eat your words"},
			} },
		{8,"You are the one who seems nervous.  Insecure.",
			{
			{-1,"Your taunts don't have any effect on me"},
			{-1,"I am calm as a pond of fish"},
			{-2,"If you fight as well as you talk, you've lost already"},
			} },
		}, -- end round 1
		{ -- round 2
		{1,"You are the newcomer?  Excited about the fights?",
			{
			{2,"Not really. I was born to do this"},
			{3,"I will show the world that I am the best"},
			{4,"And who are you? My next opponent?"},
			} },
		{2,"Be careful, I'm the next fighter.  It won't be so easy this time.",
			{
			{11,"You don't scare me"},
			{12,"Maybe I'll get some exercise this time"},
			} },
		{3,"Don't be so sure about yourself.  I'm not easy to defeat.",
			{
			{5,"Me, I can't be defeated"},
			{6,"You will see that you are"},
			{7,"You are the one who seems too sure"},
			} },
		{4,"Yes.  Next and last, because you will lose.",
			{
			{8,"Hahaha, you are a funny cow"},
			{9,"You are wrong, I am going to win"},
			{10,"I will remind you these words when you lose"},
			} },
		{5,"Everyone can be defeated.  You will this time.",
			{
			{-2,"Yes? Bring it on"},
			{-1,"You are too optimistic"},
			{-2,"That's what my last opponent said"},
			} },
		{6,"Not by a novice like you.",
			{
			{-2,"I might be unexperienced, but I am strong"},
			{-1,"Old cows like you should retire"},
			{-2,"You will feel the power of my muscles"},
			} },
		{7,"I have been training a lot, I am confident that I will win.",
			{
			{-2,"I am sorry that all that training was useless"},
			{-2,"Being confident is not enough.  You have to be strong"},
			} },
		{8,"I'm funny? Funny how? Like a clown? Do I amuse you? Am I here to amuse you?",
			{
			{-2,"Exactly.  You are a clowny cow"},
			{-1,"Get the heck out of here!"},
			{-2,"How pathetic you are is what amuses me"},
			} },
		{9,"Well, we will see in a minute.",
			{
			{-2,"Cherish these moments before your defeat"},
			{-2,"I don't need to wait, I know already"},
			{-1,"We will see how you fall"},
			} },
		{10,"Ah, you are staying with the audience for the rest of the day?",
			{
			{-1,"I will be too busy fighting the whole day"},
			{-2,"I meant that you will lose now!"},
			} },
		{11,"I don't need to scare anyone.  I will just throw you down.",
			{
			{-2,"You will not be able to"},
			{-2,"I'm like a rock, you won't move me"},
			{-1,"Be careful not to trip over on the way to the ring"},
			} },
		{12,"You mean that the last combat was no effort? I'm impressed.",
			{
			{-1,"Your sarcasm doesn't impress me, however"},
			{-2,"I can be sarcastic too: you're sooo strong.  See?"},
			{-2,"And yet that cow looked stronger than you"},
			} },
		}, -- end round 2
		{ -- round 3
			{1,"I've seen your two last matches.  Not bad for a beginner.",
			{
			{2,"I have a lot to show"},
			{3,"You're my next victim then?"},
			} },

			{2,"Why so? Did someone beat you as a calf?",
			{
			{6,"My youth is not your concern"},
			{7,"I'm going to be the one who beats you this time"},
			{8,"I'm keeping my cards close to the chest"},
			} },

			{3,"Maybe you will be my victim instead.",
			{
			{4,"Keep dreaming"},
			{5,"That is impossible, I am going to win this tournament"},
			} },

			{4,"Haha, I'm not worried about you.  You should be about me.",
			{
			{-2,"You've seen how I beat the other two without effort. You're next"},
			{-1,"You talk the talk.  Do you walk the walk?"},
			{-1,"There's two types of cows.  The ones who lose, and me."},
			} },

			{5,"And then what? What is the point of winning?",
			{
			{-2,"So that everyone will see who's best"},
			{-2,"Well, if you have doubts you can abandon now"},
			{-1,"Maybe you have a point there"},
			} },
			{6,"Sorry if I touched a weak point.  You little calves are funny.",
			{
			{-2,"You are a weak point.  I'll show you in the ring"},
			{-2,"I don't have weak points.  You don't get it"},
			{-1,"Says the old grandmother..."},
			} },

			{7,"You must have few friends with that attitude.",
			{
			{-2,"I don't need friends when I'm this good"},
			{-2,"People are envyous"},
			{-2,"Don't patronize me"},
			} },

			{8,"Playing the mysterious cow now?  You don't impress me.",
			{
			{-1,"I'm not here to impress you.  I'm here to beat you"},
			{-2,"You don't know what tricks I'm capable of"},
			{-1,"Prepare to be surprised"},
			} },
		}, -- end round 3
		{ -- round 4
			{1,"I'm impressed, you're winning all the matches.",
			{
			{2,"To be honest, I'm starting to wonder what's the point"},
			} },

			{2,"What do you mean? Do you have doubts?",
			{
			{3,"Look at it.  We're cows.  We should be giving milk, not fighting"},
			} },


			{3,"Are you trying to play mind games with me? I'm here to fight!",
			{
			{4,"So am I.  And I will beat you now"},
			{5,"Sometimes I just wonder.  Do you know what's the final prize?"},
			} },

			{4,"All those victories have only been luck.  I'll make you bite the dust!",
			{
			{-1,"You wish.   You gotta buy me dinner first!"},
			{-1,"Luck is what you will need if you want to beat me"},
			{-2,"I don't need luck.  I am this good"},
			} },

			{5,"I don't care about the prize.  I care about seeing you fall.",
			{
			{6,"Isn't it strange that we don't know what are we fighting for?"},
			{7,"Good that you don't care, because the prize will be mine"},
			} },

			{6,"Stop trying to discourage me.  It's not working.",
			{
			{-1,"I don't need to discourage you.  I'm winning anyway"},
			{-1,"That's the attitude! Maybe next year you will have a chance to win."},
			} },

			{7,"Do you care for the prize?  Your greed will make you lose.",
			{
			{-2,"Then you must have enjoyed seeing how I beat the others"},
			{-2,"Greed? I haven't talked about money"},
			{-1,"Greed, for a lack of a better word, is good"},
			} },
		}, -- end round 4
		{ -- round 5
			{1,"I heard the conversation with your last opponent. You seem worried.",
			{
			{2, "You should be the one who's worried"},
			{3, "I'm just wondering what's the point"},
			}},

			{2,"You have been lucky so far. But I'm an opponent you can't win.",
			{
			{-2,"You are so wrong that it's ridiculous"},
			{-2,"There is no opponent I can't win"},
			}},

			{3,"What's the point of anything? We're fighters. That's why we fight.",
			{
			{4,"We're raised like this, but what for?"},
			{5,"Don't you think our breeders have an agenda?"},
			}},

			{4,"Training to become fighters since our birth.  Strange.",
			{
			{6,"They want a race of supercows."},
			{7,"Is there a war outside and they want soldiers?"},
			}},


			{5,"That's an interesting perspective.  What do humans want from cows?",
			{
			{8,"Indeed.  No one told us why we have to fight"},
			}},

			{6,"What for? That doesn't make any sense.",
			{
			{9,"What else then? Why would they make us fight?"},
			{10,"I can't imagine them wanting to do us any harm"},
			}},

			{7,"I haven't heard of any war.\nI only heard about farms were cows live in peace.",
			{
			{11,"I heard that too.  They eat grass all day and give milk"},
			{12,"That's just tales for little calves"},
			}},

			{8,"Maybe the winner becomes someone's bodyguard.",
			{
			{13,"Or maybe they give you all the grass you want"},
			{14,"I hope it isn't something sinister instead"},
			}},


			{9,"You have a point.  They're selecting the best of us.",
			{
			{15,"and then...?"},
			{16,"but the more we fight, the less milk we can give"},
			}},


			{10,"They are pitching us against each other.  That's not nice.",
			{
			{17,"Should we revolt?"},
			{18,"I think that they secretly admire us"},
			}},


			{11,"Such a peaceful life.  I wonder if they are happy.",
			{
			{-1,"Maybe you want to go and join them"},
			{-2,"It's time that I beat you"},
			}},

			{12,"Well, our time has come.  We have to fight now.",
			{
			{-1,"Let's go to the arena"},
			}},

			{13,"Well, they're calling us.  Our turn to fight.",
			{
			{-1,"Let's go"},
			}},


			{14,"What do you mean?  Don't scare me.",
			{
			{19,"Honestly, I'm kind of scared too"},
			}},


			{15,"and then... I don't know.  It escapes me.",
			{
			{-2,"Well, it's time that I beat your ass"},
			{-2,"You can abandon the fight now if you feel weak"},
			}},

			{16,"They are not interested in the milk, that is clear.\nThey are looking for something else.",
			{
			{-1,"Time to fight. Let's go."},
			}},

			{17,"Start a rebellion against the humans?  That's crazy.",
			{
			{-2,"We're excellent sumo fighers.  They can't beat us"},
			{-2,"You are crazy if you're going to let them treat us like this"},
			}},

			{18,"Have you ever met a winner of the tournament?",
			{
			{20,"Actually no.  Where do they go?"},
			{-2,"I will be the next one and figure that out"},
			}},

			{19,"We should figure that out.",
			{
			{-1,"There's only one way.  Winning the tournament"},
			}},

			{20,"They are sent to the farms to enjoy their retirement.",
			{
			{-1,"You're dreaming"},
			{-2,"I want to keep fighting forever"},
			}},
		}, -- end round 5
		{ -- round 6
			{1,"Are you ready for the last match of the tournament?",
			{
			{2,"Are you?"},
			{3,"This is the moment I've been waiting for"},
			{4,"You got to ask yourself this: 'do I feel lucky?'. Do you, punk?"},
			}},

			{2,"I was born ready.  I will beat this tournament.",
			{
			{-2,"Maybe next time.  This time, I win"},
			{5,"You should have been stillborn ready"},
			}},

			{3,"You shouldn't be so eager to win.  I'm the toughest cow here.",
			{
			{6,"I have won all matches so far"},
			{7,"I want to know what's the final prize"},
			}},

			{4,"I don't need luck.  I have my muscles.",
			{
			{8,"And my axe!"},
			{-2,"You're ridiculous.  I'll beat you in five seconds"},
			}},

			{5,"That's a strong insult.  I will teach you manners in the ring.",
			{
			{-2,"I'm waiting for it"},
			}},

			{6,"So do I.  I am invincible.",
			{
			{9,"This is what happens when an unstoppable force meets\nan immovable obstacle"},
			{10,"You are not invincible.  I will show you"},
			}},


			{7,"Actually I don't know it.  What could that be?",
			{
			{11,"I was discussing that with my last opponent"},
			}},

			{8,"What are you talking about?  What axe?",
			{
			{-2,"I'm so sure I'll win that I can waste the time making jokes"},
			}},

			{9,"You are not an obstacle.  You are just a little nuisance.",
			{
			{-2,"And you are a not a force, you are a harmless breeze"},
			}},

			{10,"You have been lucky so far.  Luck will not help you this time.",
			{
			{-2,"You are going to fall so fast that you won't even see it coming"},
			}},

			{11,"And what was the conclusion?",
			{
			{12,"We couldn't figure it out.  Something sinister is going on"},
			}},

			{12,"What makes you think that it's sinister?",
			{
			{13,"Why do humans keep the prize a secret then?"},
			}},

			{13,"Good point.  But what's the goal in life, if not winning fights?",
			{
			{14,"The purpose of life?  The purpose of this tournament!"},
			{-2,"Oh, you are a philosopher. Prepare to bite the dust"},
			}},

			{14,"I will tell you once this fight is over and I win.",
			{
			{15,"We are just playing their game. We are in their hands"},
			}},

			{15,"What you're saying is dangerous.  I hope they don't hear you.",
			{
			{-2,"You are right.  Let's go fight for a last time"},
			{16,"Let's unite against them.  We are the toughest fighters.\nThe humans can't do anything against us"},
			}},

			{16,"That's insane.  Let's have our final fight instead.",
			{
			{-2,"Yes, let's fight!"},
			{17,"No, let's revolt! They can't stop us!"},
			}},

			{17,"Nonsense.  They put us here.  Don't think they can't stop a revolt.",
			{
			{18,"We are the strongest cows of the tournament.  Let's show them"},
			}},

			{18,"You convinced me.  How do we start the revolution?",
			{
			{19,"I can do it once I'm proclaimed the winner.  The herd will follow me"},
			}},

			{19,"Deal.  I will let you win.  Then, together, we will rock this stable.",
			{
			{-1,"Let's go to the ring"},
			}},
		} -- end round 6
	} -- end data

--	Insults.checkLengths()
end

function Insults.checkLengths()
	function checkLen(str)
		if game.font:getWidth(str) > 700 then print(str) end
	end
	-- this is a test, it detects strings that are too long
	for i=1,table.getn(Insults.data) do
		for j=1,table.getn(Insults.data[i]) do
			checkLen(Insults.data[i][j][2])
			for k=1,table.getn(Insults.data[i][j][3]) do
				checkLen(Insults.data[i][j][3][k][2])
			end
		end
	end
end
