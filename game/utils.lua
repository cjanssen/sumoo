-- Copyright 2013 Kirill Krysov and Christiaan Janssen
-- This file is part of Sumoo.
--
--    Sumoo is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    Sumoo is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with Sumoo.  If not, see <http://www.gnu.org/licenses/>.
function math.sign( a )
	if a >= 0 then
		return 1
	else
		return -1
	end
end

orig_newImage = love.graphics.newImage
function love.graphics.newImage(str)
	local res = orig_newImage(str)
	res:setFilter("nearest","nearest")
	return res
end

function countLines(str)
	local n=1
	for i in str:gmatch("\n") do n=n+1 end
	return n
end

function longestLine(str)
	local seq = {0,-1}
	local max = {0,1}
	local last = string.find(str,"\n")
	if not last then return str end
	while last do
		seq[1] = seq[2]+2
		seq[2] = last - 1
		if seq[2]-seq[1] > max[2]-max[1] then
			max[1] = seq[1]
			max[2] = seq[2]
		end
		last = string.find(str,"\n",last+2)
	end

	return string.sub(str,max[1],max[2])
end

function increaseExponential(dt, var, amount)
	if var < 1 then
		var = 1 - (1 - var) * math.pow(amount, 60*dt)
		if var > 0.999 then
			var = 1
		end
	end
	return var
end

function decreaseExponential(dt, var, amount)
	if var > 0 then
		var = var * math.pow(amount, 60*dt)
		if var < 0.001 then
			var = 0
		end
	end
	return var
end

function increaseExponentialSigned(dt, var, amount)
	local s = math.sign(var)
	var = math.abs(var)
	if var < 1 then
		var = 1 - (1 - var) * math.pow(amount, 60*dt)
		if var > 0.999 then
			var = 1
		end
	end
	var = var * s
	return var
end

function decreaseExponentialSigned(dt, var, amount)
	local s = math.sign(var)
	var = math.abs(var)
	if var > 0 then
		var = var * math.pow(amount, 60*dt)
		if var < 0.001 then
			var = 0
		end
	end
	var = var * s
	return var
end

function linearApprox(dt, cur, dest, spd)
	local diff = dest - cur
	if math.abs(diff) > 0 then
		local inc = math.sign(diff) * spd * dt
		if math.abs(diff) < math.abs(inc) then
			inc = diff
		end
		cur = cur + inc
	end	
	return cur
end

function mixColors(colorA, colorB, fraction)
	return { colorA[1] * fraction + colorB[1]*(1-fraction),
		colorA[2] * fraction + colorB[2]*(1-fraction),
		colorA[3] * fraction + colorB[3]*(1-fraction) }
end

function mixColorsRGB(colorA, colorB, fraction)
	local rA,gA,bA,aA = hsv2rgb(colorA[1], colorA[2], colorA[3], 255)
	local rB,gB,bB,aB = hsv2rgb(colorB[1], colorB[2], colorB[3], 255)
	local result = mixColors({rA,gA,bA}, {rB,gB,bB}, fraction)
	local h,s,v,a = rgb2hsv(result[1],result[2],result[3],255)
	return {h,s,v}
end

function hsl2rgb(h, s, l, a)
    if s<=0 then return l,l,l,a end
    h, s, l = h/360*6, s/255, l/255
    local c = (1-math.abs(2*l-1))*s
    local x = (1-math.abs(h%2-1))*c
    local m,r,g,b = (l-.5*c), 0,0,0
    if h < 1     then r,g,b = c,x,0
    elseif h < 2 then r,g,b = x,c,0
    elseif h < 3 then r,g,b = 0,c,x
    elseif h < 4 then r,g,b = 0,x,c
    elseif h < 5 then r,g,b = x,0,c
    else              r,g,b = c,0,x
    end return (r+m)*255,(g+m)*255,(b+m)*255,a
end

function hsv2rgb(h, s, v, a)
    if s <= 0 then return v,v,v,a end
    h, s, v = h/360*6, s/255, v/255
    local c = v*s
    local x = (1-math.abs((h%2)-1))*c
    local m,r,g,b = (v-c), 0,0,0
    if h < 1     then r,g,b = c,x,0
    elseif h < 2 then r,g,b = x,c,0
    elseif h < 3 then r,g,b = 0,c,x
    elseif h < 4 then r,g,b = 0,x,c
    elseif h < 5 then r,g,b = x,0,c
    else              r,g,b = c,0,x
    end 
	return (r+m)*255,(g+m)*255,(b+m)*255,a
end

function rgb2hsv(r, g, b, a)
	r,g,b = r/255, g/255, b/255
    local min = math.min(r,g,b)
	local max = math.max(r,g,b)

	local v = max
    local delta = max - min;
    if max > 0 then
        s = delta / max
	else 
		s = 0
		h = 0
		return 0,0,v*255,a
	end
	if r >= max then
		h = (g-b)/delta
	elseif g >= max then
		h = 2 + (b-r)/delta
	else
		h = 4 + (r-g)-delta
	end
    h = h * 60
    if h < 0 then
        h = h + 360
	end
    return h,s*255,v*255
end
