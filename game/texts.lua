-- Copyright 2013 Kirill Krysov and Christiaan Janssen
-- This file is part of Sumoo.
--
--    Sumoo is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    Sumoo is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with Sumoo.  If not, see <http://www.gnu.org/licenses/>.
Texts = {}

function Texts.load()
	Texts.mouseX = 0
	Texts.mouseY = 0
	Texts.mouseDown = false

	Texts.colors = {
		default = {255, 255, 255},
		hovered = {192,192,0},
		disabled = {192,192,192},
		selected = {255,255,0},
		label = {255, 255, 255}
	}
	Texts.clear()
	Texts.background = love.graphics.newImage("img/ui.png")
	Texts.setOffsets(30,430)
	Texts.spacing = 5
	Texts.deletesomething = false
end

function Texts.setOffsets(xoffset_, yoffset_)
	Texts.xoffset = xoffset_
	Texts.yoffset = yoffset_
end

function Texts.update(dt)
	Texts.mouseX = love.mouse.getX()
	Texts.mouseY = love.mouse.getY()
	local mouseDown = love.mouse.isDown("l")
	for i,v in ipairs(Texts.UIlist) do
		if mouseDown then
			Texts.clickedUI(v, Texts.mouseX, Texts.mouseY)
		else
			Texts.mouseoverUI(v, Texts.mouseX, Texts.mouseY)
		end
		Texts.updateUI(v, dt)
	end
	Texts.mouseDown = mouseDown
end

function Texts.draw()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(Texts.background, 0, 400, 0, 2, 2)
	for i,v in ipairs(Texts.UIlist) do
		Texts.drawUI(v)
	end
end


-----------------------------------------------------------------
-- 1. label
-- 2. button

function Texts.newButton(title_, x_, y_, callback_, value_)
	local lineCount = countLines(title_)
	table.insert(Texts.UIlist, {
		uitype = 2,
		title = title_,
		x = x_  + Texts.xoffset,
		y = y_  + Texts.yoffset,
		w = game.font:getWidth(longestLine(title_)),
		h = game.font:getHeight() * lineCount,
		callback = callback_,
		value = value_,
		selected = false,
		hovered = false,
		currentColor = Texts.colors.default,
		todelete = false,

		opacity = 0,
		shadeFactor = 1
	})
end

function Texts.newLabel(title_, x_, y_, color_)
	local color = color_ or Texts.colors.label
	local lineCount = countLines(title_)
	table.insert(Texts.UIlist, {
		uitype = 1,
		title = title_,
		x = x_  + Texts.xoffset,
		y = y_  + Texts.yoffset,
		w = game.font:getWidth(longestLine(title_)),
		h = game.font:getHeight() * lineCount,
		selected = false,
		hovered = false,
		currentColor = color,
		todelete = false,
		opacity = 0,
		shadeFactor = 1
	})
end

function Texts.collides(elem1, elem2)
	if elem1.x > elem2.x + elem2.w then
		return false
	end
	if elem1.x + elem1.w < elem2.x then
		return false
	end
	if elem1.y > elem2.y + elem2.h then
		return false
	end
	if elem1.y + elem1.h < elem2.y then
		return false
	end
	return true
end

function Texts.updateUI(elem, dt)
	-- active?
	if elem.uitype == 2 then
		if elem.selected then
			elem.currentColor = Texts.colors.selected
		elseif elem.hovered then
			elem.currentColor = Texts.colors.hovered
		else
			elem.currentColor = Texts.colors.default
		end
	end

	if elem.shadeFactor ~= elem.opacity then
		if elem.shadeFactor > elem.opacity then
			elem.opacity = increaseExponential(dt, elem.opacity, 0.97)
		else
			elem.opacity = decreaseExponential(dt, elem.opacity, 0.975)
		end
	end
end

function Texts.drawUI(elem)
	love.graphics.setColor(elem.currentColor[1], elem.currentColor[2], elem.currentColor[3], elem.opacity*255)
	love.graphics.print(elem.title, elem.x, elem.y)
end

function Texts.mouseoverUI(elem, x, y)
	if elem.uitype == 1 then
		return
	end

	elem.hovered = false
	if x >= elem.x and x <= elem.x + elem.w and y >= elem.y and y <= elem.y + elem.h then
		elem.hovered = true
	end

	if elem.uitype == 3 and elem.dragging then
		elem.dragging = false
	end
end

function Texts.clickedUI(elem, x, y)
	if elem.uitype == 1 then
		return
	end
	local inarea = false
	if x >= elem.x and x <= elem.x + elem.w and y >= elem.y and y <= elem.y + elem.h then
		inarea = true
	end

	if not Texts.mouseDown then
		if inarea and not elem.selected then
			elem.selected = true
			if elem.callback then				
				elem.callback(elem.value)
			end
		end
	end
end


function Texts.clear()
	Texts.UIlist = {}
	Texts.vertPos = 0
end

function Texts.appendButton(txt, option)
	Texts.newButton(txt, 0, Texts.vertPos, Texts.defaultCallback, option)
	Texts.vertPos = Texts.vertPos + Texts.UIlist[table.getn(Texts.UIlist)].h + Texts.spacing
end


function Texts.appendLabel(txt, col)
	Texts.newLabel(txt, 0, Texts.vertPos, col)
	Texts.vertPos = Texts.vertPos + Texts.UIlist[table.getn(Texts.UIlist)].h + Texts.spacing
end
